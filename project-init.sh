#!/usr/bin/with-contenv bash

echo "Running pip3 install --ignore-installed -r requirements.txt --upgrade"
pip3 install --ignore-installed -r requirements.txt --upgrade

echo "Running composer install --no-dev --no-interaction --no-ansi"
composer install --no-dev --no-interaction --no-ansi

echo "Running mkdir config.d"
mkdir config.d

echo "Running cp config.php.default config.php"
cp config.php.default config.php

echo "Running cp snmpd.conf.example /etc/snmp/snmpd.conf"
cp snmpd.conf.example /etc/snmp/snmpd.conf

echo "Running sed -i '/runningUser/d' lnms"
sed -i '/runningUser/d' lnms

echo "Running apk del build-dependencies"

apk del build-dependencies
